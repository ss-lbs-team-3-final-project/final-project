var http = require('http')
var app = require('express')()

var server = http.createServer(app)
const port = process.env.PORT || 8080
server.listen(port);
console.log('Express HTTP Server is listening at port ${port}')
app.get('/', (request, response)=> {
	console.log("Got an HTTP request, "+ __dirname)
	response.sendFile(__dirname+'/main.html')
	})


