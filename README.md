## Team Project ##

University of Dayton

Department of Computer Science

CPS 474/574 Software/Language-based Security 

Instructor, Dr Phu Phung

## User Data Monitoring through Data Scrapping##
- Using data scrapping to showcase bots that use it for various outcomes


## Team 3 - Members : ##

1. Ryan Downes, <downesr1@udayton.edu>
2. Josh Cowden, <cowdenj2@udayton.edu>

Link to trello: https://trello.com/b/ngjbVOk1/final-project

## How to Use this Project
Unzip the file. Next go to Firefox and go to the addon debugging page. 
Click on "Load Temporary Add-on" button, and select the MANIFEST.JSON file within the demo folder. 
If no problems occur in installation you should be able to now run the extentsion's functions on any site. 
Simply go to a web page and you should be prompted to choose if you will download a file or not.
